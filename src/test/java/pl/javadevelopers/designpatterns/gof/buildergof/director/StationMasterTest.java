package pl.javadevelopers.designpatterns.gof.buildergof.director;

import org.junit.Assert;
import org.junit.Test;
import pl.javadevelopers.designpatterns.gof.buildergof.builders.CargoTrainBuilder;
import pl.javadevelopers.designpatterns.gof.buildergof.builders.ExpressInterCityTrainBuilder;
import pl.javadevelopers.designpatterns.gof.buildergof.builders.SubUrbanTrainBuilder;
import pl.javadevelopers.designpatterns.gof.buildergof.builders.TrainBuilder;
import pl.javadevelopers.designpatterns.gof.buildergof.enums.TrainType;
import pl.javadevelopers.designpatterns.gof.buildergof.model.Train;

/**
 * Created 2014 by tomaszk
 */
public class StationMasterTest {

	@Test
	public void testSubUrbanTrainBuilderWillReturnPassengerNonExpressTrain() {

		TrainBuilder trainBuilder = new SubUrbanTrainBuilder();
		StationMaster stationMaster = new StationMaster(trainBuilder);
		Train train = stationMaster.getTrain();
		Assert.assertTrue(TrainType.PASSANGER.equals(train.getTrainType()) && Boolean.FALSE.equals(train.isExpressTrain()));
		System.out.println(train);
		stationMaster.setTrainBuilder(new CargoTrainBuilder());
		Train train2 = stationMaster.getTrain();
		Assert.assertTrue(TrainType.CARGO.equals(train2.getTrainType()) && Boolean.FALSE.equals(train2.isExpressTrain()));


	}

	@Test
	public void testIntercityTrainBuilderWillReturnPassengerExpressTrain() {
		TrainBuilder trainBuilder = new ExpressInterCityTrainBuilder();
		StationMaster stationMaster = new StationMaster(trainBuilder);
		Train train = stationMaster.getTrain();
		Assert.assertTrue(TrainType.PASSANGER.equals(train.getTrainType()) && Boolean.TRUE.equals(train.isExpressTrain()));
		System.out.println(train);
	}

	@Test
	public void testCargoTrainBuilderWillReturnCargoNonExpressTrain() {
		TrainBuilder trainBuilder = new CargoTrainBuilder();
		StationMaster stationMaster = new StationMaster(trainBuilder);
		Train train = stationMaster.getTrain();
		Assert.assertTrue(TrainType.CARGO.equals(train.getTrainType()) && Boolean.FALSE.equals(train.isExpressTrain()));
		System.out.println(train);
	}



}
