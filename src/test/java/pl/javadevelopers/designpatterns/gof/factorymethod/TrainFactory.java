package pl.javadevelopers.designpatterns.gof.factorymethod;

/**
 * Created 2014 by tomaszk
 */
public interface TrainFactory {

	Train getTrain();
}
