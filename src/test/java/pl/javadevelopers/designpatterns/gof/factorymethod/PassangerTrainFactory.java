package pl.javadevelopers.designpatterns.gof.factorymethod;

import pl.javadevelopers.designpatterns.gof.factorymethod.impl.PassangerTrain;

/**
 * Created 2014 by tomaszk
 */
public class PassangerTrainFactory implements TrainFactory {


	@Override
	public Train getTrain() {
		return new PassangerTrain();
	}
}
