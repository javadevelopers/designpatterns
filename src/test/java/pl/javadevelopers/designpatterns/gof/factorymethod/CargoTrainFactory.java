package pl.javadevelopers.designpatterns.gof.factorymethod;

import pl.javadevelopers.designpatterns.gof.factorymethod.impl.CargoTrain;

/**
 * Created 2014 by tomaszk
 */
public class CargoTrainFactory implements TrainFactory {


	@Override
	public Train getTrain() {
		return new CargoTrain();
	}
}
