package pl.javadevelopers.designpatterns.gof.factorymethod.impl;

import pl.javadevelopers.designpatterns.gof.factorymethod.Train;

/**
 * Created 2014 by tomaszk
 */
public abstract class AbstractTrain implements Train {

	private int carriegesNumber = 20;

	public int getCarriegesNumber() {
		return carriegesNumber;
	}
}
