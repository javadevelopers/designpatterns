package pl.javadevelopers.designpatterns.gof.factorymethod.impl;



/**
 * Created 2014 by tomaszk
 */
public class CargoTrain extends AbstractTrain {


	@Override
	public int getLocomotivesNumber() {
		return 2;
	}
}
