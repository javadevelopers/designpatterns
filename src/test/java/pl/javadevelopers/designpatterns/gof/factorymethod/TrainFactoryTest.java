package pl.javadevelopers.designpatterns.gof.factorymethod;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created 2014 by tomaszk
 */
public class TrainFactoryTest {

	@Test
	public void testFactoryMethod() {
		TrainFactory trainFactory = new PassangerTrainFactory();
		Assert.assertTrue( 1 == trainFactory.getTrain().getLocomotivesNumber());
		trainFactory = new CargoTrainFactory();
		Assert.assertTrue( 2 == trainFactory.getTrain().getLocomotivesNumber());
	}

}
