package pl.javadevelopers.designpatterns.gof.strategy;

import org.junit.Test;
import pl.javadevelopers.designpatterns.gof.strategy.strategies.impl.PrefixNamingStrategy;
import pl.javadevelopers.designpatterns.gof.strategy.strategies.impl.SufixNamingStrategy;

/**
 * Created 2014 by tomaszk
 */
public class StrategyDesignPatternTest {


	@Test
	public void testPattern() {
		StringCreator psc = new StringCreator(new PrefixNamingStrategy());
		psc.printString("?");
		psc.setNamingStrategy(new SufixNamingStrategy());
		psc.printString("?");
	}

	@Test
	public void anotherTest(){
		StringCreator.printString("?", new PrefixNamingStrategy());
	}

}
