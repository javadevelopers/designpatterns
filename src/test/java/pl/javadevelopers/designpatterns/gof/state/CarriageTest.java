package pl.javadevelopers.designpatterns.gof.state;

import org.junit.Test;
import pl.javadevelopers.designpatterns.gof.state.model.Carriage;
import pl.javadevelopers.designpatterns.gof.state.impl.CarriageAvailable;
import pl.javadevelopers.designpatterns.gof.state.impl.CarriageDamaged;
import pl.javadevelopers.designpatterns.gof.state.impl.CarriageInManeuverDisposition;
import pl.javadevelopers.designpatterns.gof.state.impl.CarriageUnavailable;
import pl.javadevelopers.designpatterns.gof.state.towerofifs.StatusEnum;
import pl.javadevelopers.designpatterns.gof.state.towerofifs.Wagon;

/**
 * Created 2014 by tomaszk
 */
public class CarriageTest {


	/** Example showing code with State design pattern to implement different behaviour of an object when it's state change */
	@Test
	public void exampleStateDesignPattern() {

		Carriage carriageOne = new Carriage.Builder()
				.quickExampleCarriage()
				.state(new CarriageAvailable())
				.build();
		carriageOne.reportStatus();

		Carriage carriageTwo = new Carriage.Builder().
				quickExampleCarriage().
				state(new CarriageUnavailable())
				.build();
		carriageTwo.reportStatus();

		Carriage carriageThree = new Carriage.Builder().
				quickExampleCarriage().
				state(new CarriageDamaged())
				.build();
		carriageThree.reportStatus();

		Carriage carriageFour = new Carriage.Builder().
				quickExampleCarriage().
				state(new CarriageInManeuverDisposition())
				.build();
		carriageFour.reportStatus();

	}


	/**
	 *  Example showing common implementation without State Design Pattern called "The Tower of Ifs Design Pattern"
	 *  if you don't know what i mean see {@link pl.javadevelopers.designpatterns.gof.state.towerofifs.Wagon#raportujStatus()}
	 *
	 *  Try to guess what would happen when ten new states of Carriage(Wagon) will be implemented in near future;
	 * .... yes , then more if else cases ..... awful ... but very common
	 *
	 */
	@Test
	public void exampleAntiPattern() {
		Wagon wagon = new Wagon(1213, 1, StatusEnum.ROZPORZADZALNY,"BOC-1234");
		System.out.println(wagon.raportujStatus());

		Wagon wagon2 = new Wagon(1213, 1, StatusEnum.USZKODZONY,"BOC-1234");
		System.out.println(wagon2.raportujStatus());

		Wagon wagon3 = new Wagon(1213, 1, StatusEnum.NIEDOSTEPNY,"BOC-1234");
		System.out.println(wagon3.raportujStatus());

		Wagon wagon4 = new Wagon(1213, 1, StatusEnum.W_DYSP_MANEWR,"BOC-1234");
		System.out.println(wagon4.raportujStatus());
	}

}
