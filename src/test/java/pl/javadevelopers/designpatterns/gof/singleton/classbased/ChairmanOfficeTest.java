package pl.javadevelopers.designpatterns.gof.singleton.classbased;

import org.junit.Test;

/**
 * Created 2014 by tomaszk
 */
public class ChairmanOfficeTest {

	@Test
	public void testSingletonInstanceWillReturnTheSameObject() {

		try {
			ChairmanOffice chairmanOffice = null;
			for (int i = 0; i < 20 ; i++) {
				chairmanOffice = ChairmanOffice.getInstance();
				System.out.println("YOU: Can you pay me a little more ????\n");
				boolean isSalaryRised = chairmanOffice.askForRise("123456");
				if (isSalaryRised) {
					System.out.println("BOSS: OK\n");
				} else {
					System.out.println("BOSS: GET OUT OF MY FACE YOU LAZY MAXXXXXXXX\n");
				}
			}
		} catch (Exception e) {
			System.out.println("*************************************************");
			System.out.println(e.getMessage());
			System.out.println("*************************************************");
		}
	}

}
