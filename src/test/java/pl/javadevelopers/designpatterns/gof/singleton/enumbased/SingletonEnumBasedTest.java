package pl.javadevelopers.designpatterns.gof.singleton.enumbased;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created 2014 by tomaszk
 */
public class SingletonEnumBasedTest {

	@Test
	public void testSingletonEnumBasedCounterWillReferenceAlwaysToTheSameObject() {
		System.out.println("sigleton enum based");
		SingletonEnumBased singletonEnumBased = null;
		for (int i = 0; i < 20 ; i++) {
			singletonEnumBased = SingletonEnumBased.INSTANCE;
			singletonEnumBased.addOne();
			singletonEnumBased = null;
		}
		singletonEnumBased = SingletonEnumBased.INSTANCE;
		Assert.assertTrue(singletonEnumBased.getCount() == 20);
	}

}
