package pl.javadevelopers.designpatterns.gof.decorator.decorators;

import org.junit.Test;
import pl.javadevelopers.designpatterns.gof.decorator.component.Compartment;
import pl.javadevelopers.designpatterns.gof.decorator.component.Reservable;

/**
 * Created 2014 by tomaszk
 */
public class AbstractDecoratorTest {

	@Test
	public void doTestOfDecorator() {
		Reservable compartment = Compartment.getStandardCompartment();
		compartment.printOffer();

		Reservable firstClass = new FirstClassCompartmentDecorator(compartment);
		firstClass.printOffer();

		Reservable businessClass = new BusinessClassCompartmentDecorator(firstClass);
		businessClass.printOffer();

	}

	@Test
	public void anotherTest() {
		Reservable compartment = new BusinessClassCompartmentDecorator(new FirstClassCompartmentDecorator(Compartment.getStandardCompartment()));
		compartment.printOffer();
	}

}
