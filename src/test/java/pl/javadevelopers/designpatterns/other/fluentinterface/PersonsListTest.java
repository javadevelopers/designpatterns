package pl.javadevelopers.designpatterns.other.fluentinterface;

import org.junit.Test;

import static org.junit.Assert.*;

public class PersonsListTest {

    @Test
    public void test_fluent_interface() {
        PersonsList.newPersonsList()
                .add("ala")
                .add("ma")
                .add("kota")
                .remove("ala")
                .printPersonsList();
    }

}