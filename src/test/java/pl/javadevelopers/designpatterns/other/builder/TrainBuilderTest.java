package pl.javadevelopers.designpatterns.other.builder;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created 2014 by tkieron
 */
public class TrainBuilderTest {

	@Test
	public void testBuild() {
		Train train = new Train.Builder(2)
				.composedOnTrack(1)
				.destinationTrack(2)
				.build();
		System.out.println(train);
		Assert.assertFalse(train.isExpressTrain());

	}


}
