package pl.javadevelopers.designpatterns.other.nullobject;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created 2014 by tomaszk
 */
public class NullObjectPassangerTest {

	@Test
	public void testNullObject() {
		AbstractPassenger passenger1 = PassengerFactory.getPassenger("Joe");
		AbstractPassenger passenger2 = PassengerFactory.getPassenger("Rob");
		AbstractPassenger passenger3 = PassengerFactory.getPassenger("Tom");

		System.out.println("Passengers list");
		System.out.println(passenger1.getName());
		System.out.println(passenger2.getName());
		/** In case that method {@link pl.javadevelopers.designpatterns.other.nullobject.PassengerFactory#getPassenger(String)}
		 *  returns null instead of NullPassenger a NullPointerException will be thrown
		 */
		System.out.println(passenger3.getName());

		Assert.assertTrue(passenger3.isNil());

	}

}
