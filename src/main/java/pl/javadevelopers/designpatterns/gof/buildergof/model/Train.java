package pl.javadevelopers.designpatterns.gof.buildergof.model;

import pl.javadevelopers.designpatterns.gof.buildergof.enums.TrainType;

/**
 * Build model object
 *
 * Created 2014 by tomaszk
 */
public class Train {

	private int locomotivesNumber;

	private int carriagesNumber;

	private boolean expressTrain;

	private TrainType trainType;

	public int getLocomotivesNumber() {
		return locomotivesNumber;
	}

	public void setLocomotivesNumber(int locomotivesNumber) {
		this.locomotivesNumber = locomotivesNumber;
	}

	public int getCarriagesNumber() {
		return carriagesNumber;
	}

	public void setCarriagesNumber(int carriagesNumber) {
		this.carriagesNumber = carriagesNumber;
	}

	public boolean isExpressTrain() {
		return expressTrain;
	}

	public void setExpressTrain(boolean expressTrain) {
		this.expressTrain = expressTrain;
	}

	public TrainType getTrainType() {
		return trainType;
	}

	public void setTrainType(TrainType trainType) {
		this.trainType = trainType;
	}

	@Override
	public String toString() {
		return "Train{" +
				"locomotivesNumber=" + locomotivesNumber +
				", carriagesNumber=" + carriagesNumber +
				", expressTrain=" + expressTrain +
				", trainType=" + trainType +
				'}';
	}
}
