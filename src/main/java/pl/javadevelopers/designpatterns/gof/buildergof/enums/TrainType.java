package pl.javadevelopers.designpatterns.gof.buildergof.enums;

/**
 * Created 2014 by tomaszk
 */
public enum TrainType {

	PASSANGER, CARGO

}
