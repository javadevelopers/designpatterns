package pl.javadevelopers.designpatterns.gof.buildergof.director;

import pl.javadevelopers.designpatterns.gof.buildergof.builders.TrainBuilder;
import pl.javadevelopers.designpatterns.gof.buildergof.model.Train;

/**
 *
 * Director Class
 *
 * Created 2014 by tomaszk
 */
public class StationMaster {

	private TrainBuilder trainBuilder;

	public StationMaster(TrainBuilder trainBuilder) {
		this.trainBuilder = trainBuilder;
	}

	public void setTrainBuilder(TrainBuilder trainBuilder) {
		this.trainBuilder = trainBuilder;
	}

	public Train getTrain() {
		return trainBuilder.build();
	}

}
