/**
 * 
 * Example Implementation of classical GOF Builder Design Pattern
 * 
 * @{pl.javadevelopers.designpatterns.gof.buildergof.director.StationMaster} is a Director Class
 * and {@link pl.javadevelopers.designpatterns.gof.buildergof.builders.TrainBuilder} is an abstract builder
 * that is extended by concrete builders responsible for creating objects of dedicated trains.
 *
 * Running code in {@link pl.javadevelopers.designpatterns.gof.buildergof.director.StationMasterTest}
 *
 * @see http://javadevelopers.pl/wzorce-projektowe-builder/
 *
 * Created 2014 by tomaszk
 */
package pl.javadevelopers.designpatterns.gof.buildergof;