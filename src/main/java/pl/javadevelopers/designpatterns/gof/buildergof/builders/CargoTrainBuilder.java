package pl.javadevelopers.designpatterns.gof.buildergof.builders;


import pl.javadevelopers.designpatterns.gof.buildergof.enums.TrainType;
import pl.javadevelopers.designpatterns.gof.buildergof.model.Train;

/**
 * Created 2014 by tomaszk
 */
public class CargoTrainBuilder extends TrainBuilder {

	public void composeTrain() {
		train = new Train();
		train.setCarriagesNumber(30);
		train.setLocomotivesNumber(2);
		train.setTrainType(TrainType.CARGO);
	}

	public Train build() {
		composeTrain();
		return train;
	}

}
