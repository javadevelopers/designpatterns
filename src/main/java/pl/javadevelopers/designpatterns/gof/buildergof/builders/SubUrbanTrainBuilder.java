package pl.javadevelopers.designpatterns.gof.buildergof.builders;


import pl.javadevelopers.designpatterns.gof.buildergof.enums.TrainType;
import pl.javadevelopers.designpatterns.gof.buildergof.model.Train;

/**
 * Created 2014 by tomaszk
 */
public class SubUrbanTrainBuilder extends TrainBuilder {

	public void composeTrain() {
		train = new Train();
		train.setCarriagesNumber(20);
		train.setLocomotivesNumber(1);
		train.setTrainType(TrainType.PASSANGER);
	}

	public Train build() {
		composeTrain();
		return train;
	}




}
