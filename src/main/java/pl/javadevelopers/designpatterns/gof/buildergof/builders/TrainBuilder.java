package pl.javadevelopers.designpatterns.gof.buildergof.builders;

import pl.javadevelopers.designpatterns.gof.buildergof.model.Train;

/**
 *
 * Abstract Builder Class
 *
 * Created 2014 by tomaszk
 */
public abstract class TrainBuilder {

	protected Train train;

	protected abstract void composeTrain();

	public Train build() {
		return train;
	}




}
