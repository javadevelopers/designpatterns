package pl.javadevelopers.designpatterns.gof.strategy.strategies;

/**
 *
 * Strategy interface that has to be implemented by all strategy classes
 * Created 2014 by tomaszk
 */
public interface NamingStrategy {

	String VALUE_TO_ADD = " Hello World ";

	String rename(String str);
}
