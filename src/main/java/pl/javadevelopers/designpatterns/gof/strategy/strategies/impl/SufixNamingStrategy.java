package pl.javadevelopers.designpatterns.gof.strategy.strategies.impl;

import pl.javadevelopers.designpatterns.gof.strategy.strategies.NamingStrategy;

/**
 * Created 2014 by tomaszk
 */
public class SufixNamingStrategy implements NamingStrategy {

	@Override
	public String rename(String str) {
		if (str == null || str.trim().length() == 0) {
			throw new IllegalArgumentException("Given String cannot be null or empty");
		}
		return str +  VALUE_TO_ADD;
	}
}
