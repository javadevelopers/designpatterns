package pl.javadevelopers.designpatterns.gof.strategy;

import pl.javadevelopers.designpatterns.gof.strategy.strategies.NamingStrategy;

/**
 * A class that prints given String deciding how it should look by given Strategy.
 *
 * Created 2014 by tomaszk
 */
public class StringCreator {

	private NamingStrategy namingStrategy;

	public StringCreator(NamingStrategy namingStrategy) {
		this.namingStrategy = namingStrategy;
	}


	public void printString(String str) {
		System.out.println(namingStrategy.rename(str));
	}

	/** Alternative to pass strategy as method parameter */
	public static void printString(String str, NamingStrategy strategy) {
		System.out.println(strategy.rename(str));
	}

	public void setNamingStrategy(NamingStrategy namingStrategy) {
		this.namingStrategy = namingStrategy;
	}



}
