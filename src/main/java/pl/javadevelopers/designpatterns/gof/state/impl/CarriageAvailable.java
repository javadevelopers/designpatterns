package pl.javadevelopers.designpatterns.gof.state.impl;

import pl.javadevelopers.designpatterns.gof.state.model.Carriage;
import pl.javadevelopers.designpatterns.gof.state.CarriageState;

/**
 * Created 2014 by tomaszk
 */
public class CarriageAvailable implements CarriageState {

	@Override
	public void reportPosition(Carriage carriage) {
		System.out.println("wagon rozporządzalny oczekuje na bocznicy : " + carriage.getSidingNumber() + " na torze " + carriage.getTrack());
	}
}
