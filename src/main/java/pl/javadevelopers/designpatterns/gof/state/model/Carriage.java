package pl.javadevelopers.designpatterns.gof.state.model;

import pl.javadevelopers.designpatterns.gof.state.CarriageState;

/**
 *
 * Repressenting Carriage, constructed by nested Builder
 * with quickExampleCarriage for quick template object creation;
 *
 * Note {@link #state} field that is representing the state of object and {@link #reportStatus()} method that is implemented with
 * the state design pattern.
 *
 * {@see CarriageTest } - run code in JUnit and compere it with the "tower of Ifs anti pattern"
 *
 * Created 2014 by tomaszk
 */
public class Carriage {

	private Carriage(){

	}

	private String number;

	private String track;

	private String sidingNumber;

	private CarriageState state;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTrack() {
		return track;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	public String getSidingNumber() {
		return sidingNumber;
	}

	public void setSidingNumber(String sidingNumber) {
		this.sidingNumber = sidingNumber;
	}

	public void reportStatus() {
		if (state != null) {
			state.reportPosition(this);
		} else {
			System.out.println("status unknown");
		}
	}

	public void setState(CarriageState state) {
		this.state = state;
	}

	public static class Builder {

		private String number;
		private String track;
		private String sidingNumber;
		private CarriageState state;


		public Builder quickExampleCarriage() {
			this.track = "1";
			this.sidingNumber = "BOC-123";
			this.number = "112";
			return this;
		}

		public Builder state(CarriageState state) {
			this.state = state;
			return this;
		}

		public Carriage build() {
			Carriage carriage = new Carriage();
			carriage.setNumber(number);
			carriage.setSidingNumber(sidingNumber);
			carriage.setTrack(track);
			carriage.setState(state);
			return carriage;
		}
	}
}
