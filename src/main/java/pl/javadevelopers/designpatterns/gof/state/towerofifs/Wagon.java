package pl.javadevelopers.designpatterns.gof.state.towerofifs;

/**
 * Created 2014 by tomaszk
 */
public class Wagon {

	public Wagon(int numer, int tor, StatusEnum stratus, String bocznica) {
		this.numer = numer;
		this.tor = tor;
		this.status = stratus;
		this.bocznica = bocznica;
	}

	private int numer;

	private int tor;

	private StatusEnum status;

	private String bocznica;

	public int getNumer() {
		return numer;
	}

	public void setNumer(int numer) {
		this.numer = numer;
	}

	public int getTor() {
		return tor;
	}

	public void setTor(int tor) {
		this.tor = tor;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public String getBocznica() {
		return bocznica;
	}

	public void setBocznica(String bocznica) {
		this.bocznica = bocznica;
	}

	public String raportujStatus() {
		if (status == null) {
			return "Nieznany";
		} else if (StatusEnum.ROZPORZADZALNY.equals(status)) {
			return "wagon rozporządzalny oczekuje na bocznicy : " + bocznica + " na torze " + tor;
		} else if (StatusEnum.NIEDOSTEPNY.equals(status)) {
			return "wagon jest niedostępny";
		} else if (StatusEnum.USZKODZONY.equals(status)) {
			return "Wagon w naprawie";
		} else if (StatusEnum.W_DYSP_MANEWR.equals(status)) {
			return "wagon realizuje dyspozycje manewrowa";
		} else {
			return "statusu nie rozpoznano";
		}
	}
}
