package pl.javadevelopers.designpatterns.gof.state;

import pl.javadevelopers.designpatterns.gof.state.model.Carriage;

/**
 *
 * Basic interface that has to be implemented in all states
 *
 * Created 2014 by tomaszk
 */
public interface CarriageState {

	void reportPosition(Carriage carriage);

}
