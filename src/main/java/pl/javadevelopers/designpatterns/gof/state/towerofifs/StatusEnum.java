package pl.javadevelopers.designpatterns.gof.state.towerofifs;

/**
 * Created 2014 by tomaszk
 */
public enum  StatusEnum {

	USZKODZONY,ROZPORZADZALNY,NIEDOSTEPNY,W_DYSP_MANEWR;

}
