package pl.javadevelopers.designpatterns.gof.state.impl;

import pl.javadevelopers.designpatterns.gof.state.model.Carriage;
import pl.javadevelopers.designpatterns.gof.state.CarriageState;

/**
 * Created 2014 by tomaszk
 */
public class CarriageUnavailable implements CarriageState {



	@Override
	public void reportPosition(Carriage carriage) {
		System.out.println("wagon jest niedostepny - w trakcie realizacji przewozu");
	}
}
