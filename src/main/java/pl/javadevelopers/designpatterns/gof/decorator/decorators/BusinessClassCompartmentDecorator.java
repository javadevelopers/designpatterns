package pl.javadevelopers.designpatterns.gof.decorator.decorators;

import pl.javadevelopers.designpatterns.gof.decorator.component.Reservable;

import java.math.BigDecimal;

/**
 *
 * Decorating Compartment as a BusinessClass changing properties values
 *
 * Created 2014 by tomaszk
 *
 *
 */
public class BusinessClassCompartmentDecorator extends AbstractCompartmentDecorator {


	public BusinessClassCompartmentDecorator(Reservable component) {
		super(component.getCompartment());
		markAsFirstClass();
	}


	@Override
	public void printOffer() {
		compartment.printOffer();
	}

	private void markAsFirstClass() {
		compartment.setSeatsNumber(4);
		compartment.setWifi(true);
		compartment.setAirCondition(true);
		compartment.setCompartmentClass("1 Biznes");
		compartment.setPrice(new BigDecimal(25));
	}
}
