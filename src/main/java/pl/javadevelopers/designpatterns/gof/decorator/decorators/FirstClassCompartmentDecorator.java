package pl.javadevelopers.designpatterns.gof.decorator.decorators;

import pl.javadevelopers.designpatterns.gof.decorator.component.Reservable;

import java.math.BigDecimal;

/**
 * Marks a compartment as a first class
 * Changing properties values in Compartment
 *
 * Created 2014 by tomaszk
 */
public class FirstClassCompartmentDecorator extends AbstractCompartmentDecorator {


	public FirstClassCompartmentDecorator(Reservable component) {
		super(component.getCompartment());
		markAsFirstClass();
	}


	@Override
	public void printOffer() {
		compartment.printOffer();
	}

	private void markAsFirstClass() {
		compartment.setSeatsNumber(6);
		compartment.setWifi(true);
		compartment.setAirCondition(false);
		compartment.setCompartmentClass("1");
		compartment.setPrice(new BigDecimal(15));
	}
}
