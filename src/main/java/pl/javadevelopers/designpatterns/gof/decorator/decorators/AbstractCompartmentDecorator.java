package pl.javadevelopers.designpatterns.gof.decorator.decorators;

import pl.javadevelopers.designpatterns.gof.decorator.component.Compartment;
import pl.javadevelopers.designpatterns.gof.decorator.component.Reservable;

/**
 * The Abstract Decorator is a root for all Decorators
 * and a placeholder for an object of Compartment that is being decorated
 *
 * Created 2014 by tomaszk
 */
public abstract class AbstractCompartmentDecorator implements Reservable {

	protected Compartment compartment;

	public AbstractCompartmentDecorator(Compartment compartment) {
		this.compartment = compartment;
	}

	@Override
	public Compartment getCompartment() {
		return compartment;
	}
}
