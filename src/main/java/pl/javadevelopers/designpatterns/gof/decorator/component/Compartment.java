package pl.javadevelopers.designpatterns.gof.decorator.component;

import java.math.BigDecimal;

/**
 * Class representing a Single Compartment in carriage
 * @
 * Created 2014 by tomaszk
 */
public class Compartment implements Reservable {

	private int seatsNumber;

	private boolean airCondition;

	private boolean wifi;

	private BigDecimal price;

	private String compartmentClass;

	private Compartment(int seatsNumber, boolean airCondition, boolean wifi, BigDecimal price, String compartmentClass) {
		this.seatsNumber = seatsNumber;
		this.airCondition = airCondition;
		this.wifi = wifi;
		this.price = price;
		this.compartmentClass = compartmentClass;
	}

	public static Compartment getStandardCompartment() {
		  return new Compartment(8, false, false, new BigDecimal(10.50),"2");
	}

	@Override
	public void printOffer() {
		System.out.println("Miejsca w klasie " + getCompartmentClass() );
		System.out.print(" łącznie miejsc w przedziale " + getSeatsNumber());
		System.out.print(isAirCondition() ? " przedzial klimatyzowany" : "");
		System.out.print(isWifi() ? " dostęp do sieci wifi" : "");
		System.out.print(" cena biletu: " + getPrice());
		System.out.println();
	}

	@Override
	public Compartment getCompartment() {
		return this;
	}

	public int getSeatsNumber() {
		return seatsNumber;
	}

	public void setSeatsNumber(int seatsNumber) {
		this.seatsNumber = seatsNumber;
	}

	public boolean isAirCondition() {
		return airCondition;
	}

	public void setAirCondition(boolean airCondition) {
		this.airCondition = airCondition;
	}

	public boolean isWifi() {
		return wifi;
	}

	public void setWifi(boolean wifi) {
		this.wifi = wifi;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getCompartmentClass() {
		return compartmentClass;
	}

	public void setCompartmentClass(String compartmentClass) {
		this.compartmentClass = compartmentClass;
	}
}
