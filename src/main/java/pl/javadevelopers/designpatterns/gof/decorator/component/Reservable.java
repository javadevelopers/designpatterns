package pl.javadevelopers.designpatterns.gof.decorator.component;

/**
 * Created 2014 by tomaszk
 */
public interface Reservable {

	/** Core bussines method of our Compartment object */
	void printOffer();

	/** Key concept of the pattern getCompartment method called in all Decorator Constructors
	 * allows us to pass the Decorator to the constructor of the other decorator as well as Compartment by it self
	 */
	Compartment getCompartment();



}
