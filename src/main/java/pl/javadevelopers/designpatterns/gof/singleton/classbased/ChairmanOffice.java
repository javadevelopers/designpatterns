package pl.javadevelopers.designpatterns.gof.singleton.classbased;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Singleton <b>thread safe</b> thanks to nested class that is referenced ( and loaded ) not
 * earlier then the moment that getInstanceMethod() is called
 *
 *
 * Created 2014 by tomaszk
 */
public class ChairmanOffice {

	private ChairmanOffice() {
	}

	private Random random = new Random();

	private Map<String, Integer> requestCounter = new HashMap<String, Integer>();;

	public boolean askForRise(String employeeId) throws IllegalArgumentException {

		boolean requestAccepted = random.nextBoolean();
		if (requestCounter.containsKey(employeeId)) {
			int count = requestCounter.get(employeeId);
			if (count > 4) {
				throw new IllegalArgumentException("BOSS: You have ask for salary rise to many times , YOU ARE FIRED\n");
			}
			count++;
			requestCounter.put(employeeId,count);
		} else {
			requestCounter.put(employeeId, 1);
		}
		return requestAccepted;
	}

	private static class SingletonHolder {
		public static final ChairmanOffice INSTANCE = new ChairmanOffice();
	}

	public static ChairmanOffice getInstance() {
		return SingletonHolder.INSTANCE;
	}

}
