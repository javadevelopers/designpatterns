package pl.javadevelopers.designpatterns.gof.singleton.enumbased;

/**
 * Singleton implementation based on Enum
 * Enums are of-the-box thread safe, and cannot be instantiated by calling default constructors.
 * In effect we are guaranteed that only one instance of class will be created
 *
 * Created 2014 by tomaszk
 */
public enum SingletonEnumBased {

	INSTANCE;

	private int count;

	public void addOne() {
		count++;
	}

	public int getCount() {
		return count;
	}
}
