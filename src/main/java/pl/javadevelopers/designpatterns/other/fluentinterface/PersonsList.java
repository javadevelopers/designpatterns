package pl.javadevelopers.designpatterns.other.fluentinterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by Tomasz Kieroński on 25.01.15.
 */
public class PersonsList {

    private List<String> persons;

    private PersonsList() {
        persons = new ArrayList<String>();
    }

    public static PersonsList newPersonsList() {
        return new PersonsList();
    }

    public PersonsList add(String person) {
        persons.add(person);
        return this;
    }

    public PersonsList remove(String person) {
        persons.remove(person);
        return this;
    }

    public void printPersonsList() {
        persons.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });
    }

}
