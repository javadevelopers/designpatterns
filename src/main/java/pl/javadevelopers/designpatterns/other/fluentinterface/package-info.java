/**
 * Introduced by Eric Evans & Martin Fowler
 * @see http://martinfowler.com/bliki/FluentInterface.html
 *
 * Created by Tomasz Kieroński on 25.01.15.
 */
package pl.javadevelopers.designpatterns.other.fluentinterface;