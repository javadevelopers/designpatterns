package pl.javadevelopers.designpatterns.other.nullobject;

/**
 * Created 2014 by tomaszk
 */
public class PassengerFactory {

	public static final String[] withTickets = {"Rob", "Joe", "Julie"};

	/**
	 * If @param name equals one from the list a Passenger object will be returned other wise a NullPassenger will be returned
	 *
	 * @param name
	 * @return
	 */
	public static AbstractPassenger getPassenger(String name){
		for (int i = 0; i < withTickets.length; i++) {
			if (withTickets[i].equalsIgnoreCase(name)){
				return new Passenger(name);
			}
		}
		return new NullPassenger();
	}

}
