package pl.javadevelopers.designpatterns.other.nullobject;

/**
 *
 * Can be implemented also as an interface if necessary
 *
 *
 *
 * Created 2014 by tomaszk
 */
public abstract class AbstractPassenger {

	protected String name;

	/** Indicates wheather an object is null or not */
	public abstract boolean isNil();

	/** Get passenger name */
	public abstract String getName();

}
