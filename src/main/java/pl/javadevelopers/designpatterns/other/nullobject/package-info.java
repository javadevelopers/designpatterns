/**
 *
 * Example implementation of NullObjectDesign Pattern
 *
 * Can be used to avoid returning null from any method that returns object
 * following some conditions.
 *
 * In an example a Passenger will be returned only if his name is on
 * {@link pl.javadevelopers.designpatterns.other.nullobject.PassengerFactory#withTickets} array
 *
 * Otherwise a null ( NullObject will be returned )
 *
 * This simple design pattern can help us to avoid one of most commons RuntimeException : the NullPointerException
 *
 * @see http://javadevelopers.pl/wzorce-projektowe-null-object/
 *
 * Created 2014 by tomaszk
 */
package pl.javadevelopers.designpatterns.other.nullobject;