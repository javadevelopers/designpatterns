package pl.javadevelopers.designpatterns.other.nullobject;

/**
 * An Object representing a non null state
 *
 * Created 2014 by tomaszk
 */
public class Passenger extends AbstractPassenger {

	public Passenger(String name) {
		this.name = name;
	}

	@Override
	public boolean isNil() {
		return false;
	}

	@Override
	public String getName() {
		return name;
	}
}
