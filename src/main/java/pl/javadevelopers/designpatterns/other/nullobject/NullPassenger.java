package pl.javadevelopers.designpatterns.other.nullobject;

/**
 * An Object representing a null state
 *
 *
 * Created 2014 by tomaszk
 */
public class NullPassenger extends AbstractPassenger {

	public NullPassenger() {
	}

	@Override
	public boolean isNil() {
		return true;
	}

	@Override
	public String getName() {
		return "OBJECT IS NULL";
	}
}
